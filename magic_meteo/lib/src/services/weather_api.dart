import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:magic_meteo/src/model/station_model.dart';
import 'package:magic_meteo/src/model/weather_prediction_model.dart';

class WeatherApi {
  Future<List<WeatherPrediction>> getWeatherForecast(
      List<Station> favoriteStations) async {
    List<WeatherPrediction> stationsWeather = [];

    for (var station in favoriteStations) {
      var endpoint = Uri.parse(
          'https://api.open-meteo.com/v1/forecast?latitude=${station.lat}&longitude=${station.lng}&daily=weathercode,apparent_temperature_max,apparent_temperature_min&forecast_days=6&timezone=Europe%2FBerlin');
      final weatherResponse = await http.get(endpoint);
      final weatherData = jsonDecode(weatherResponse.body);

      List<String> conditions = [];
      for (var i = 0; i < weatherData['daily']['weathercode'].length; i++) {
        int weatherCode = weatherData['daily']['weathercode'][i];
        conditions.add(getCondition(weatherCode));
      }

      WeatherPrediction weatherPrediction =
          WeatherPrediction.fromJson(station, weatherData, conditions);

      print(
          "Weather prediction for ${station.name}: ${weatherPrediction.condition} with temperatures between ${weatherPrediction.minTemperature} and ${weatherPrediction.maxTemperature} °C");
      stationsWeather.add(weatherPrediction);
    }

    return stationsWeather;
  }

  Future<Station> getWeatherLightForecast(Station station) async {
    var endpointSnowfall = Uri.parse(
        'https://api.open-meteo.com/v1/forecast?latitude=${station.lat}&longitude=${station.lng}&past_days=31&daily=snowfall_sum,weathercode,apparent_temperature_max,apparent_temperature_min&hourly=snow_depth&timezone=Europe%2FBerlin');

    final snowfallResponse = await http.get(endpointSnowfall);
    final weatherData = jsonDecode(snowfallResponse.body);

    // compute the number of days since last snowfall
    List<double> snowfallSum =
        weatherData['daily']['snowfall_sum'].cast<double>();
    int daysSinceLastSnowfall = -1;

    for (var i = snowfallSum.length - 1; i >= 0; i--) {
      if (snowfallSum[i] > 0.0) {
        daysSinceLastSnowfall = snowfallSum.length - i;
        break;
      }
    }
    station.snowfall = daysSinceLastSnowfall;

    int nbDays = weatherData['daily']['time'].length - 1;
    double minTemperature =
        weatherData['daily']['apparent_temperature_min'][nbDays];
    double maxTemperature =
        weatherData['daily']['apparent_temperature_max'][nbDays];
    station.temperature =
        "${minTemperature.toInt()} | ${maxTemperature.toInt()} °C";

    int weatherCode = weatherData['daily']['weathercode'][0];
    station.condition = getCondition(weatherCode);

    var endpointSnowdepth = Uri.parse(
        'https://api.open-meteo.com/v1/forecast?latitude=${station.lat}&longitude=${station.lng}&forecast_days=1&hourly=snow_depth&timezone=Europe%2FBerlin');

    final snowdepthResponse = await http.get(endpointSnowdepth);
    final snowdepthData = jsonDecode(snowdepthResponse.body);

    double snowDepth = snowdepthData['hourly']['snow_depth'][0];
    station.snowdepth = snowDepth;

    print(
        "Weather light prediction for ${station.name}: ${station.condition} with temperatures between ${station.temperature} °C and ${station.snowfall} days since last snowfall");

    return station;
  }
}

String getCondition(int weatherCode) {
  if (weatherCode == 0) {
    return "sunny";
  } else if (weatherCode == 1 || weatherCode == 2 || weatherCode == 3) {
    return "cloudy";
  } else if ([
    45,
    48,
    51,
    53,
    55,
    56,
    57,
    61,
    63,
    65,
    66,
    67,
    80,
    81,
    82,
    85,
    86,
    95,
    96,
    99
  ].contains(weatherCode)) {
    return "rainy";
  } else if ([71, 73, 75, 77].contains(weatherCode)) {
    return "snowy";
  } else {
    return "cloudy";
  }
}
