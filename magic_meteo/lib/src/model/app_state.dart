import 'package:flutter/material.dart';
import 'package:magic_meteo/src/model/station_model.dart';
import 'package:magic_meteo/src/services/weather_api.dart';
import 'package:shared_preferences/shared_preferences.dart';

//         We will use our AppState model as a global app state. To notify
//         children widgets when the data has changed, we need to extends
//         ChangeNotifier class. This model will be "provided" to the children.
//         You can look at the updateTodo method below to see how to notify
//         widgets that listen to our model. Next step -> go to app.dart

class AppState extends ChangeNotifier {
  List<Station> stations;
  List<Station> favorites = [];

  AppState({required this.stations}) {
    _getFromSharedPref().then((favStations) {
      for (var station in stations) {
        if (favStations.contains(station.name)) {
          favorites.add(station);
        }
      }
      notifyListeners();
    });
  }

  void addToFavorite(List<Station> stations) async {
    for (var station in stations) {
      Station computedStation =
          await WeatherApi().getWeatherLightForecast(station);
      favorites.add(computedStation);
      _addToSharedPref(station.name!);
    }
    notifyListeners();
  }

  void removeFavorite(Station station) {
    favorites.remove(station);
    _removeFromSharedPref(station.name!);
    notifyListeners();
  }
}

// Return the list of favorite stations from shared preferences
Future<List<String>> _getFromSharedPref() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  List<String>? favouriteStations = prefs.getStringList('stations');
  return favouriteStations ?? [];
}

_addToSharedPref(String stationToAdd) async {
  SharedPreferences.getInstance().then((prefs) {
    List<String> stations = prefs.getStringList('stations') ?? [];
    stations.add(stationToAdd);
    stations = stations.toSet().toList();
    prefs.setStringList('stations', stations);
    print(stations);
  });
}

// Remove the stations that are already in the favorites
_removeFromSharedPref(String stationToRemove) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  List<String> favStations = prefs.getStringList('stations') ?? [];
  favStations.remove(stationToRemove);
  prefs.setStringList('stations', favStations);
  print(favStations);
}

enum AppTab { home, weather, map }
