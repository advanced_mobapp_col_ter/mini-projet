class Station {
  final String? name;
  final double? lat;
  final double? lng;
  double? snowdepth = 0.0; // actual snow depth
  int? snowfall = 0; // nb of days since last snowfall
  String? temperature;
  String? condition;

  Station(this.name, this.lat, this.lng);
}
