// Weather model

import 'package:flutter/material.dart';

/// A model that represents the weather at a given location.
//https://api.open-meteo.com/v1/forecast?latitude=52.52&longitude=13.41&hourly=temperature_2m,relativehumidity_2m,snowfall,visibility,windspeed_10m&forecast_days=1&timezone=Europe%2FBerlin

/*
{"latitude":52.52,"longitude":13.419998,"generationtime_ms":0.34606456756591797,"utc_offset_seconds":3600,"timezone":"Europe/Berlin","timezone_abbreviation":"CET",
"elevation":38.0,"hourly_units":{"time":"iso8601","temperature_2m":"°C","relativehumidity_2m":"%","snowfall":"cm","visibility":"m","windspeed_10m":"km/h"},
"hourly":{"time":["2023-03-20T00:00","2023-03-20T01:00","2023-03-20T02:00","2023-03-20T03:00","2023-03-20T04:00","2023-03-20T05:00","2023-03-20T06:00","2023-03-20T07:00",
"2023-03-20T08:00","2023-03-20T09:00","2023-03-20T10:00","2023-03-20T11:00","2023-03-20T12:00","2023-03-20T13:00","2023-03-20T14:00","2023-03-20T15:00","2023-03-20T16:00",
"2023-03-20T17:00","2023-03-20T18:00","2023-03-20T19:00","2023-03-20T20:00","2023-03-20T21:00","2023-03-20T22:00","2023-03-20T23:00"],
"temperature_2m":[10.5,10.2,9.6,9.2,8.7,8.2,7.9,7.9,8.4,8.9,9.9,10.9,11.1,11.4,11.4,11.4,11.2,11.0,10.4,9.8,9.1,8.7,8.4,8.3],
"relativehumidity_2m":[88,84,85,87,92,94,94,94,92,90,83,71,70,68,67,67,68,68,69,70,74,76,75,73],
"snowfall":[0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00],
"visibility":[24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00,24140.00],"windspeed_10m":[3.3,4.3,5.1,4.2,5.1,4.5,5.8,5.2,9.8,8.2,8.8,15.0,13.3,15.1,15.7,13.4,10.4,11.3,9.1,7.6,6.9,5.9,5.8,6.5]}}
 */
class WeatherModel {
  final String? cityName;

  WeatherModel({this.cityName});

  factory WeatherModel.fromJson(Map<String, dynamic> json) {
    return WeatherModel(
      cityName: "Arconciel"
    );
  }
}
