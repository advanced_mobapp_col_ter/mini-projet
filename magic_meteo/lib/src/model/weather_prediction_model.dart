// Weather prediction model for a magic station
import 'package:magic_meteo/src/model/station_model.dart';

class WeatherPrediction {
  final double latitude;
  final double longitude;
  final String name;
  final List<String> dates;
  final List<double> minTemperature;
  final List<double> maxTemperature;
  final List<String> condition;

  WeatherPrediction({
    required this.latitude,
    required this.longitude,
    required this.name,
    required this.dates,
    required this.minTemperature,
    required this.maxTemperature,
    required this.condition,
  });

  factory WeatherPrediction.fromJson(
      Station station, Map<String, dynamic> json, List<String> condition) {
    return WeatherPrediction(
      latitude: station.lat!,
      longitude: station.lng!,
      name: station.name!,
      dates: json['daily']['time'].cast<String>(),
      condition: condition,
      minTemperature: json['daily']['apparent_temperature_min'].cast<double>(),
      maxTemperature: json['daily']['apparent_temperature_max'].cast<double>(),
    );
  }
}
