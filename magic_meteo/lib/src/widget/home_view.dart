import 'package:flutter/material.dart';
import 'package:magic_meteo/src/model/app_state.dart';
import 'package:magic_meteo/src/widget/home_widget.dart';
import 'package:provider/provider.dart';
import '../model/station_model.dart';

// This class contains all HomeView widgets in functions of the favorite stations
class HomeView extends StatefulWidget {
  // Constructor
  const HomeView({Key? key}) : super(key: key);

  @override
  HomeViewState createState() => HomeViewState();
}

class HomeViewState extends State<HomeView> {
  final List<Station> _selectedStations = [];
  List<Station> _filteredStations = [];

  void _updateFilteredStations(String search) {
    final favoriteStations =
        Provider.of<AppState>(context, listen: false).favorites;
    final stations = Provider.of<AppState>(context, listen: false)
        .stations
        .where((station) => !favoriteStations.contains(station))
        .toList();
    setState(() {
      _filteredStations = stations
          .where((station) =>
              station.name!.toLowerCase().contains(search.toLowerCase()))
          .where((station) => !_selectedStations.contains(station))
          .toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    final favoriteStations = Provider.of<AppState>(context).favorites;
    final updatedStationsState = Provider.of<AppState>(context)
        .stations
        .where((station) => !favoriteStations.contains(station))
        .toList();

    if (_filteredStations.isEmpty) {
      _filteredStations = Provider.of<AppState>(context)
          .stations
          .where((station) => !favoriteStations.contains(station))
          .toList();
    }

    if (favoriteStations.isEmpty) {
      return Padding(
          padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerRight,
                child: ElevatedButton(
                  onPressed: () {
                    openDialogFav(context);
                  },
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(32.0),
                    ),
                  ),
                  child: const Icon(Icons.add),
                ),
              ),
              const Text('Add your favorite stations'),
            ],
          ));
    }

    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerRight,
              child: ElevatedButton(
                onPressed: () {
                  openDialogFav(context);
                  _filteredStations = updatedStationsState;
                },
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(32.0),
                  ),
                ),
                child: const Icon(Icons.add),
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: favoriteStations.length,
                itemBuilder: (context, index) {
                  final station = favoriteStations[index]!;
                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 4),
                    child: Dismissible(
                      key: Key(station.name!),
                      // Provide a unique key for each item
                      direction: DismissDirection.endToStart,
                      background: Container(
                        color: Colors.red,
                        alignment: Alignment.centerLeft,
                        padding: const EdgeInsets.only(left: 16),
                        child: const Icon(Icons.delete, color: Colors.white),
                      ),
                      onDismissed: (direction) {
                        // Remove the station from the favoriteStations list
                        setState(() {
                          Provider.of<AppState>(context, listen: false)
                              .removeFavorite(station);
                        });
                      },
                      child: HomeWidget(
                        station: station,
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  void openDialogFav(BuildContext context) {
    _updateFilteredStations('');
    showDialog(
      context: context,
      builder: (context) => Dialog(
        child: StatefulBuilder(builder: (context, setState) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              // Search bar to find a station
              TextField(
                onChanged: (search) {
                  setState(() {
                    _updateFilteredStations(
                        search); // Met à jour la liste des stations filtrées
                  });
                },
                decoration: const InputDecoration(
                  labelText: 'Search',
                  border: OutlineInputBorder(),
                ),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: _filteredStations.length,
                  itemBuilder: (context, index) {
                    final station = _filteredStations[index];
                    print(_selectedStations.contains(station));
                    return CheckboxListTile(
                      title: Text(station.name!),
                      value: _selectedStations.contains(station),
                      onChanged: (bool? selected) {
                        setState(() {
                          print(selected);
                          if (selected!) {
                            _selectedStations.add(station);
                          } else {
                            _selectedStations.remove(station);
                          }
                        });
                      },
                    );
                  },
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                    onPressed: () {
                      // Close the dialog
                      setState(() {
                        _selectedStations.clear();
                      });
                      Navigator.of(context).pop();
                    },
                    child: const Text('CANCEL'),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                      // Add selected stations to favorites and close the dialog
                      Provider.of<AppState>(context, listen: false)
                          .addToFavorite(
                        _selectedStations.toList(),
                      );
                      setState(() {
                        _selectedStations.clear();
                      });
                    },
                    child: const Text('ADD'),
                  ),
                ],
              ),
            ],
          );
        }),
      ),
    );
  }

}
