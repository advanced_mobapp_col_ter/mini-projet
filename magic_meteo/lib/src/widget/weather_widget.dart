import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:magic_meteo/src/model/app_state.dart';
import 'package:magic_meteo/src/model/weather_prediction_model.dart';
import 'package:magic_meteo/src/services/weather_api.dart';
import 'package:provider/provider.dart';

class WeatherWidget extends StatefulWidget {
  // Constructor
  const WeatherWidget({
    Key? key,
  }) : super(key: key);

  @override
  WeatherWidgetState createState() => WeatherWidgetState();
}

class WeatherWidgetState extends State<WeatherWidget> {
  @override
  Widget build(BuildContext context) {
    WeatherApi client = WeatherApi();
    final favoriteStations = Provider.of<AppState>(context).favorites;

    return FutureBuilder<List<WeatherPrediction>>(
        future: client.getWeatherForecast(favoriteStations),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return _buildWeatherPage(snapshot.data!);
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return const Center(child: CircularProgressIndicator());
        });
  }

  Widget _buildWeatherPage(List<WeatherPrediction> stationsWeather) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: stationsWeather.length,
      itemBuilder: (context, index) {
        final WeatherPrediction station = stationsWeather[index];
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          child: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Color(0xFF4F4F4F), Color(0xFF222222)],
              ),
              borderRadius: BorderRadius.all(Radius.circular(15)),
            ),
            child: Card(
              color: Colors.transparent,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Text(
                        station.name,
                        style: const TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    const SizedBox(height: 12),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: List.generate(6, (dayIndex) {
                        final String condition = station.condition[dayIndex];
                        final double minTemp = station.minTemperature[dayIndex];
                        final double maxTemp = station.maxTemperature[dayIndex];
                        return Container(
                          width: (MediaQuery.of(context).size.width - 30) / 6,
                          child: Column(
                            children: [
                              Text(
                                DateFormat.E()
                                    .format(
                                        DateTime.parse(station.dates[dayIndex]))
                                    .substring(0, 2),
                                style: const TextStyle(
                                    fontSize: 11, color: Colors.white),
                                textAlign: TextAlign.center,
                              ),
                              const SizedBox(height: 4),
                              SvgPicture.asset(
                                "assets/icons/$condition.svg",
                                width: 25,
                                height: 25,
                              ),
                              const SizedBox(height: 4),
                              Text(
                                '${minTemp.toStringAsFixed(0)}° | ${maxTemp.toStringAsFixed(0)}°',
                                style: const TextStyle(
                                    fontSize: 11, color: Colors.white),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        );
                      }),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

/*
    return Column(
      children: const [
        /*Container(
          padding: const EdgeInsets.all(10),
          height: 60,
          width: 60,
          decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Image.asset("assets/icons/add.svg"),
        ),
        const SizedBox(
          height: 8.0,
        ),
        const Text(
          "Test",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),*/
      ],
    );
  }
*/
