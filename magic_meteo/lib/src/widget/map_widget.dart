import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:latlong2/latlong.dart';
import 'package:location/location.dart';
import '../model/station_data.dart';
import '../model/station_model.dart';

class MapWidget extends StatelessWidget {
  final List<Station> favoriteStations;

  const MapWidget({
    Key? key,
    required this.favoriteStations,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> myStationsNames = StationData.stationsNames;
    List<Station> myStations = StationData.stations;
    // Retrieve the location of the user
    return FutureBuilder(
        future: _getPosition(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            LocationData data = snapshot.data as LocationData;
            return FlutterMap(
              options: MapOptions(
                center: LatLng(data.latitude!, data.longitude!),
                // Add a pin to the user's location
                zoom: 10.0,
                // block rotation
                interactiveFlags: InteractiveFlag.all & ~InteractiveFlag.rotate,
              ),
              layers: [
                TileLayerOptions(
                  urlTemplate:
                      "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                  subdomains: ['a', 'b', 'c'],
                ),
                MarkerLayerOptions(
                  markers: [
                    //Add marker for user's location
                    Marker(
                      width: 80.0,
                      height: 80.0,
                      point: LatLng(data.latitude!, data.longitude!),
                      builder: (ctx) => Container(
                        child: const Icon(
                          Icons.location_on,
                          color: Colors.blue,
                        ),
                      ),
                    ),
                    // Set markers for each station
                    for (var i = 0; i < myStationsNames.length; i++)
                      // Retrieve station with the name
                      Marker(
                        width: 80.0,
                        height: 80.0,
                        point: LatLng(myStations[i].lat!, myStations[i].lng!),
                        builder: (BuildContext ctx) {
                          return GestureDetector(
                            onTap: () {
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text(
                                    myStationsNames[i],
                                    textAlign: TextAlign.center,
                                  ),
                                  duration: const Duration(seconds: 1),
                                ),
                              );
                            },
                            child: Container(
                              child: const Icon(
                                Icons.location_on,
                                color: Colors.red,
                              ),
                            ),
                          );
                        },
                      ),
                  ],
                ),
              ],
            );
          }
          return const Center(
            child: CircularProgressIndicator.adaptive(),
          );
        });
  }
}

_getPosition() async {
  Location location = new Location();

  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;

  _serviceEnabled = await location.serviceEnabled();
  if (!_serviceEnabled) {
    _serviceEnabled = await location.requestService();
    if (!_serviceEnabled) {
      return;
    }
  }

  _permissionGranted = await location.hasPermission();
  if (_permissionGranted == PermissionStatus.denied) {
    _permissionGranted = await location.requestPermission();
    if (_permissionGranted != PermissionStatus.granted) {
      return;
    }
  }

  _locationData = await location.getLocation();
  print(_locationData);
  return _locationData;
}
