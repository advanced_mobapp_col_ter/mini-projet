import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:magic_meteo/src/model/station_model.dart';
import 'package:magic_meteo/src/services/weather_api.dart';

class HomeWidget extends StatelessWidget {
  final Station station;

  const HomeWidget({
    Key? key,
    required this.station,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WeatherApi client = WeatherApi();
    return FutureBuilder<Station>(
      future: client.getWeatherLightForecast(station),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return _buildHomePage(snapshot.data!);
        } else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const Center(child: CircularProgressIndicator());
      },
    );
  }
}

class _buildHomePage extends StatelessWidget {
  final Station station;

  const _buildHomePage(this.station);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            color: Colors.grey[900],
            borderRadius: BorderRadius.circular(15),
          ),
          child: Column(
            children: [
              // align text to the left
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  station.name!,
                  style: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const SizedBox(height: 8),
                        if (station.condition != null)
                          SvgPicture.asset(
                            "assets/icons/${station.condition}.svg",
                            width: 30,
                            height: 30,
                          ),
                        const SizedBox(height: 8),
                        Text(
                          "${station.temperature}",
                          style: const TextStyle(
                              color: Colors.white, fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      SvgPicture.asset(
                        "assets/icons/snow-depth.svg",
                        width: 30,
                        height: 30,
                      ),
                      const SizedBox(width: 4),
                      Text(
                        "${station.snowdepth!} m",
                        style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Text(
                        "Latest snowfall",
                        style: TextStyle(color: Colors.white),
                      ),
                      const SizedBox(height: 4),
                      if (station.snowfall == 0)
                        const Text(
                          "Today",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      else if (station.snowfall == 1)
                        const Text(
                          "Yesterday",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      else if (station.snowfall == -1 ||
                          station.snowfall! >= 31)
                        const Text(
                          "Over one month",
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      else
                        Text(
                          "${station.snowfall} days ago",
                          style: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
